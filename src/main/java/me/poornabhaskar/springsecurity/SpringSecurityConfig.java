package me.poornabhaskar.springsecurity;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.ldap.core.LdapTemplate;
import org.springframework.ldap.core.support.BaseLdapPathContextSource;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.Customizer;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.ldap.LdapBindAuthenticationManagerFactory;
import org.springframework.security.web.SecurityFilterChain;

@Configuration
public class SpringSecurityConfig {

    @Bean
    SecurityFilterChain securityFilterChain(HttpSecurity http) throws Exception {
        http.authorizeHttpRequests((authorize) -> authorize.anyRequest().fullyAuthenticated())
                .formLogin(Customizer.withDefaults());

        return http.build();
    }

    public SpringSecurityConfig(LdapTemplate ldapTemplate) {
        ldapTemplate.setIgnorePartialResultException(true);

    }

    @Bean
    AuthenticationManager authenticationManager(BaseLdapPathContextSource baseLdapPathContextSource){
        LdapBindAuthenticationManagerFactory ldapBindAuthenticationManagerFactory=new LdapBindAuthenticationManagerFactory(baseLdapPathContextSource);
        //ldapBindAuthenticationManagerFactory.setUserSearchFilter("(&(objectCategory=person)(SamAccountName={0}))");
        ldapBindAuthenticationManagerFactory.setUserDnPatterns("uid={0}");
        return ldapBindAuthenticationManagerFactory.createAuthenticationManager();
    }
}
